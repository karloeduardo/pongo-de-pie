/*************************************
    CODIGO DE CONTROL DEL GRID FLUIDO
*************************************/
var $container, 
    init            =   "", 
    $md             =   false, 
    $mt             =   false, 
    $ms             =   false, 
    cubo            =   1,
    banner          =   0,
    portrait        =   0, 
    inicio          =   0, 
    nvueltas        =   0, 
    pagination      =   null, 
    layuot_options  =   null, 
    mq_desktop      =   null, 
    mq_tablet       =   null, 
    mq_movil        =   null, 
    maxitems        =   0,
    secondArr       =   {},
    counter         =   0;
/*************************************
    BEGIN: ARRAY DE SECUENCIA INICIAL
*************************************/
var firstArr = [
    ["169", "300x169", "mm-amarillo"],
    ["225", "300x225", ""],
    ["400", "300x400", "mm-amarillo"],
    ["225", "300x225", ""],
    ["169", "300x169", ""],
    ["tesugerimos", "300x400", "mm-amarillo"],
    ["400", "300x400", "mm-amarillo"],
    ["169", "300x169", ""],
    ["225", "300x225", ""],
    ["portrate", "300x600", ""],
    ["225", "300x225", ""],
    ["negro", "300x169", "mm_negro"],
    ["400", "300x400", "mm-amarillo"],
    ["225", "300x225", ""],
    ["225", "300x225", ""]
];
/*************************************
    END: ARRAY DE SECUENCIA INICIAL
*************************************/

/*************************************
    BEGIN: ARRAY DE SECUENCIA HARD CODE
*************************************/
var secondArr = [
    ["169", "300x169", "um-basic"],
    ["225", "300x225", "um-basic um-basic2"],
    /*["box_banner", "300x250", "um-basic"],*/
    ["225", "300x225", "um-basic"],
    ["169", "300x169", "um-basic um-basic2"],
    ["400", "300x400", "um-basic"],
    /*["400", "300x400", "um-basic"],*/
    ["169", "300x169", "um-basic um-basic2"],
    ["225", "300x225", "um-basic"],
    ["400", "300x400", "um-basic um-basic2"],
    ["225", "300x225", "um-basic"],
    /*["negro", "300x169", "um-basic"],*/
    ["400", "300x400", "um-basic um-basic2"],
/*    ["169", "300x169", "um-basic"],*/
    ["169", "300x169", "um-basic"],
    ["superbanner", "", ""]
];
/*************************************
    END: ARRAY DE SECUENCIA HARD CODE
*************************************/

/*************************************
    BEGIN: MICRO PLUGIN

    MICRO PLUGIN DETECTOR DE EVENTO RESIZE
    DE LOS ITEMS DEL GRID FLUIDO 
*************************************/
(function($) {
    var elems = $([]),
        timeout_id;
    $.event.special.resize = {
        setup: function() {
            var elem = $(this);
            elems = elems.add(elem);
            elem.data('resize', {
                w: elem.width(),
                h: elem.height()
            });
            if (elems.length === 1) {
                poll();
            }
        },
        teardown: function() {
            var elem = $(this);
            elems = elems.not(elem);
            elem.removeData('resize');
            if (!elems.length) {
                clearTimeout(timeout_id);
            }
        }
    };
    function poll() {
        elems.each(function() {
            var elem = $(this),
                width = elem.width(),
                height = elem.height(),
                data = elem.data('resize');
            if (width !== data.w || height !== data.h) {
                data.w = width;
                data.h = height;
                elem.triggerHandler('resize');
            }
        });
        timeout_id = setTimeout(poll, 250);
    }
})(jQuery);
/*************************************
    END: MICRO PLUGIN
*************************************/

/*************************************
    STAR: CARGA SCRIPT JS
*************************************/
function loadScript (c, d) {
    /* APPEND SCRIPT JS */
    var e = document.createElement("script");
    e.setAttribute("type", "text/javascript");
    e.setAttribute("src", c);
    "undefined" != typeof d && e.setAttribute("charset", d);
    document.getElementsByTagName("body")[0].appendChild(e);
    return !0;
}
/*************************************
    END: CARGA SCRIPT JS
*************************************/
var mv = {};
mv.init = function() {
    $('.mv-relacionado-container').owlCarousel({
        items: 1,
        navigation: true,
        customNavigation: true,
        lazyLoad: true,
        controlClass: "owl-nav",
        navClass: ["mv-prev", "mv-next"],
        responsive: true,
        singleItem: true,
        mouseDrag: false,
        touchDrag: false
    });

    if($(window).width() > 948){
        $(".mv-elemento-relacionado").each(function(){
                $(this).children("h4").hover(function(){
                    $(this).parent().toggleClass("hovered");
                });
            
        });
    }
}
/*************************************
    BEGIN: OBJETO BANNER, COMMUNITIES, OTOS
*************************************/
var func = {
    bannerInit: function (idCubo){
        /* CARGA BANNER CUBO */
        if ( typeof(googletag) !== 'undefined') {
            googletag.cmd.push(function() {
                var medidasBox = googletag.sizeMapping().
                addSize([980,140],[300,250]).
                addSize([740, 140], [728,90]).
                addSize([320, 140], [320, 50]).
                build();
                googletag.defineSlot(adUnit, [300,250],idCubo).defineSizeMapping(medidasBox).addService(googletag.pubads()).setTargeting("position","middle-btf");
                googletag.display(idCubo);
            });
            cubo++;
        }
    },
    bannerPortraitInit: function (idPortrait) {
        /* CARGA BANNER PORTRAIT */
        if ( typeof(googletag) !== 'undefined') {
            googletag.cmd.push(function() {
                var medidasPortrait = googletag.sizeMapping().
                addSize([980,140],[300,600]). //Desktop and landscape.
                addSize([740, 140], []). // /Ipad.
                addSize([320, 140], []). // Iphones.
                build();
                googletag.defineSlot(adUnit, [300,600],idPortrait).defineSizeMapping(medidasPortrait).addService(googletag.pubads()).setTargeting("position","middle-btf");
                googletag.display(idPortrait);
            });
            portrait++;
        }
    },
    mediaDesktop: function () {
        /* CARGA BANNER EN DESKTOP */   
        idCubo = 'banner_box_'+cubo;
        $(".box_banner_"+cubo).html('<div class="banner-grid"><article class="publicidad"><p>PUBLICIDAD</p><div id="'+idCubo+'" class="mm-img"></div></article></div>');
        func.bannerInit(idCubo);
        func.mediaDesktopPortrait();
    },
    mediaDesktopPortrait: function() {
        idPortrait = 'portrait_banner_'+portrait;
        if($(".portrait_banner_"+portrait).length){
            $(".portrait_banner_"+portrait).html('<div class="banner-grid"><article class="publicidad"><p>PUBLICIDAD</p><div id="'+idPortrait+'" class="mm-img"></div></article></div>');
            func.bannerPortraitInit(idPortrait);
        }
    },
    mediaTablet: function () {
        /* CARGA BANNER EN TABLET */
        idCubo = 'banner_box_'+cubo;
        $(".box_banner_"+cubo).html('<div class="banner-grid"><article class="publicidad"><p>PUBLICIDAD</p><div id="'+idCubo+'" class="mm-img"></div></article></div>');
        func.bannerInit(idCubo);
    },
    mediaSmartPhone: function () {
        /* CARGA BANNER EN SMARTPHONE */
        $(".box_banner").html('<img class="mm-img" href="#" src="../../../assets/jumper/images/banner1.png" width="300" height="225">');
    },
    media_desktop: function (mediaDesktop){
        /* VERIFICA QUE BANNER CARGAR EN DESKTOP */
        if (mediaDesktop && typeof(mediaDesktop) === "function") {
            if (!$md) {
                mediaDesktop();
                $md=true;
                $mt=false;
                $ms=false;
            }
        }
    },
    media_tablet: function (mediaTablet){
        /* VERIFICA QUE BANNER CARGAR EN TABLET */
        if (mediaTablet && typeof(mediaTablet) === "function") {
            if (!$mt) {
                mediaTablet();
                $md=false;
                $mt=true;
                $ms=false;
            }
        }
    },
    media_smartphone: function (mediaSmartPhone){
        /* VERIFICA QUE BANNER CARGAR EN SMARTPHONE */
        if (mediaSmartPhone && typeof(mediaSmartPhone) === "function") {
            if (!$ms) {
                mediaSmartPhone();
                $md=false;
                $mt=false;
                $ms=true;
            }
        }
    },
    share_reload: function () {
        /* CARGA COMINITIES */
        try{
            socialShare.indexNotesExpanded();    
        }catch(e){
            console.log(e);
        }
    },
    comunities_load: function () {
        /* CARGA SCRIPTS COMUNITIES SOLO SI ES NECESARIO */
        try{
            var interval_comunities, contador=0;
            loadScript('http://comentarios.esmas.com/js/communities.js');
            function comm (){
                contador++; 
                if ("undefined" == typeof init_comunidades) { 
                    viewing.init(); 
                    clearTimeout(interval_comunities); 
                }
                if (contador > 20)
                    clearTimeout(interval_comunities);
            }
            interval_comunities = setInterval( comm, 3000);
        }catch(e){
            console.log(e);
        }
    },
    quitar_biografias: function(text) {
        var text = text.toLowerCase(); // a minusculas
            text = text.replace('biografias/', '');
      return text;
    }
}
/*************************************
    END: OBJETO BANNER, COMMUNITIES, OTOS
*************************************/

/*************************************
    BEGIN: OBJETO GRID FLUIDO
*************************************/
var gridFluido = {
    colWidth: function() {
        /*************************************
            CALCULO DE LOS ANCHOS DE CADA CAJA
        *************************************/
        var columnNum = 1,
            columnWidth = 0,
            columnWidths = 0;
        w = $container.width();
        var margenes = eval($container.data("margins"));
        if (mq_desktop === null) return false;
        if (w > mq_desktop) {
            columnNum = 3;
            marginst = margenes[0][0];
            marginsl = margenes[0][1];
        } else if (w > mq_tablet) {
            columnNum = 3;
            marginst = margenes[1][0];
            marginsl = margenes[1][1];
        } else if (w >= mq_movil) {
            columnNum = 2;
            marginst = margenes[2][0];
            marginsl = margenes[2][1];
        }
        $container.find('.item').each(function() {
            var $item = $(this),
                ivisible = true,
                ww = $container.width();
            columnWidths = (ww - (marginsl * (columnNum - 1))) / columnNum;
            var column = eval($item.data('options'));
            var wd = $container.width();
            if (wd > mq_desktop) {
                columns = column[0];
                ivisible = column[3];
            } else if (wd > mq_tablet) {
                columns = column[1];
                ivisible = column[4];
            } else if (wd > mq_movil) {
                columns = column[2];
                ivisible = column[5];
            }
            if (columns == 3) {
                width = (columnWidths * columnNum) + (marginsl*2);
            } else if (columns == 2) {
                width = (columnWidths * columnNum);
            } else if (columns == 1) {
                width = (columnWidths);
            }
            if (wd < mq_desktop) {
                $container.css({
                    "padding-right": marginsl,
                    "padding-left": marginsl
                });
            }else {
                $container.css({
                    "padding": 0
                });
            }
            if (ivisible) {
                $item.css({
                    width: width,
                    'margin-bottom': marginst
                }).removeClass('h0');
            } else {
                $item.css({
                    width: 0,
                    transform: 'none',
                    margin: 0
                }).addClass('h0');
            }
        });
        return columnWidth;
    },
    widthGutter: function() {
        /*************************************
            CALCULO DE MARGENES Y BREAKPIONT
        *************************************/
        var columnNum = 1;
        w = $container.width(), index = 0, marginl = 0;
        var margenes = eval($container.data("margins"));
        if (mq_desktop === null) return false;
        if (w > mq_desktop) {
            columnNum = 3;
            marginst = margenes[0][0];
            marginsl = margenes[0][1];
            func.media_desktop(func.mediaDesktop);
        } else if (w > mq_tablet) {
            columnNum = 3;
            marginst = margenes[1][0];
            marginsl = margenes[1][1];
            func.media_tablet(func.mediaTablet);
        } else if (w >= mq_movil) {
            columnNum = 2;
            marginst = margenes[2][0];
            marginsl = margenes[2][1];
            func.media_smartphone(func.mediaSmartPhone);
        }
        return marginsl;
    },
    grid: function() {
        /*************************************
            INICIO DEL GRID
        *************************************/
        $container.isotope({
            itemSelector: '.item',
            masonry: {
                columnWidth: gridFluido.colWidth(),
                gutter: gridFluido.widthGutter()
            }
        });
    },
    loadData: function(e) {
        /*************************************
            CARGA DE CONTENIDOS DEL GRID
        *************************************/
        /*AJUSTES*/
        $('.btn_container').hide();
        $(".loader").css("display", "inline-block");
        var ritems = 0,
            ultimo = 0,
            bandera = 0,
            arr_vueltas = 0;
        var layuot_options = eval($container.data("options")),
            mq_desktop = parseInt(layuot_options[0]),
            mq_tablet = parseInt(layuot_options[1]),
            mq_movil = parseInt(layuot_options[2]),
            w = $container.width();
        if (w > mq_desktop) {
            pagination = eval($container.data('desktop'));
        } else if (w > mq_tablet) {
            pagination = eval($container.data('tablet'));
        } else if (w > mq_movil) {
            pagination = eval($container.data('smartphone'));
        }
        pagination_items = parseInt(pagination[0]);
        pagination_vueltas = parseInt(pagination[1]);
        maxitems = pagination_items;
        
        /*AJUSTES*/
        e.preventDefault();
        var $results, num = 0;
        var irefresh = 0;
        $md=false;
        $mt=false;
        $ms=false;
         $('.btn_container').css("padding-top", "55px");
         /*************************************
            CARGA DEL GSA
         *************************************/
        $.gsa({
            GSA_domain: "http://googleak.esmas.com/search",
            GSA_query: $container.data('query'),
            GSA_num: maxitems,
            GSA_client: $container.data('client'),
            GSA_site: $container.data('site'),
            GSA_requiredfields: $container.data('requiredfields'),
            GSA_start: inicio,
            GSA_partialfields: $container.data('partialfields'),
        }, function(data) {
            if (data.RES) {
                $results = $.gsa.results(data);
                ritems += pagination_items;
                var elem = '',
                    finalizo = 0,
                    ww = 0,
                    hh = 0,
                    $image, tipo = "",
                    layuot_options = eval($container.data("options")),
                    mq_desktop = parseInt(layuot_options[0]),
                    mq_tablet = parseInt(layuot_options[1]),
                    mq_movil = parseInt(layuot_options[2]);
                if ($results) {
                    nvueltas++;
                    for (i = 0; i < maxitems; i++) {
                        if (i <= (eval(data.RES["@attributes"].EN) - eval(data.RES["@attributes"].SN))) {
                            var title = $results[i]['title'], urls = $results[i]['URL'], icono_vf ="", category = (($results[i]['category']).toLowerCase().indexOf("biograf") > -1 ) ? "Famosos" : $results[i]['category'], categoryurl = (($results[i]['categoryUrl']).toLowerCase().indexOf("biograf") > -1 ) ? func.quitar_biografias($results[i]['categoryUrl']) : $results[i]['categoryUrl'];
                            if (typeof $container.data("icons_vf") !== 'undefined') {
                                if (typeof $results[i]["videosRelacionado"] !== 'undefined') {
                                    icono_vf = '<div class="mm-icon-container"><i class="tvsagui-video"></i></div>';
                                }else if (typeof $results[i]["galeriasRelacionada"] !== 'undefined') {
                                    icono_vf = '<div class="mm-icon-container"><i class="tvsagui-foto"></i></div>';
                                }
                            }

                            /*************************************
                                CONSULTA SI ES UN INDICE
                            *************************************/
                            if (init && nvueltas === 1) {
                                arr = firstArr;
                            } else {
                                arr = secondArr;
                            }
                            
                            if ((bandera) >= (arr.length)) {
                                bandera = 0;
                                arr_vueltas = i;
                            }

                            for (j = 0; j < arr.length; j++) {
                                if (arr[j + bandera][0] == '169') {
                                    $image = (typeof $results[i]['300x165'] !== 'undefined') ? $results[i]['300x165'] : (typeof $results[i]['300x150'] !== 'undefined') ? $results[i]['300x150'] : $results[i]['120x90'];
                                    ww = 300;
                                    hh = 169;
                                    tipo = arr[j + bandera][2];
                                    ultimo = arr.length * nvueltas;
                                    break;
                                } else if (arr[j + bandera][0] == '225') {
                                    $image = (typeof $results[i]['300x226'] !== 'undefined') ? $results[i]['300x226'] : (typeof $results[i]['300x150'] !== 'undefined') ? $results[i]['300x150'] : $results[i]['120x90'];
                                    ww = 300;
                                    hh = 225;
                                    tipo = arr[j + bandera][2];
                                    ultimo = arr.length * nvueltas;
                                    break;
                                } else if (arr[j + bandera][0] == '400') {
                                    $image = (typeof $results[i]['300x400'] !== 'undefined') ? $results[i]['300x400'] : (typeof $results[i]['300x150'] !== 'undefined') ? $results[i]['300x150'] : $results[i]['120x90'];
                                    ww = 300;
                                    hh = 400;
                                    tipo = arr[j + bandera][2];
                                    ultimo = arr.length * nvueltas;
                                    break;
                                 } else if (arr[j + bandera][0] == "box_banner") {
                                    $image = (typeof $results[i]['300x250'] !== 'undefined') ? $results[i]['300x250'] : (typeof $results[i]['300x150'] !== 'undefined') ? $results[i]['300x150'] : $results[i]['120x90'];
                                    ww = 300;
                                    hh = 250;
                                    tipo = "box_banner";
                                    ultimo = arr.length * nvueltas;
                                    break;
                                } else if (arr[j + bandera][0] == "superbanner") {
                                    //$image = (typeof $results[i]['300x250'] !== 'undefined') ? $results[i]['300x250'] : (typeof $results[i]['300x150'] !== 'undefined') ? $results[i]['300x150'] : $results[i]['120x90'];
                                    if ($(window).width() > 947) {
                                        ww = 300;
                                        hh = 250;
                                    }else if ($(window).width() > 648 && $(window).width() <= 947) {
                                        ww = 728;
                                        hh = 90;
                                    }else if ($(window).width() > 320 && $(window).width() <= 647) {
                                        ww = 320;
                                        hh = 50;
                                    }
                                    tipo = "superbanner";
                                    ultimo = arr.length * nvueltas;
                                    break;
                                } else if (arr[j + bandera][0] == "portrate") {
                                    $image = (typeof $results[i]['300x400'] !== 'undefined') ? $results[i]['300x400'] : (typeof $results[i]['300x150'] !== 'undefined') ? $results[i]['300x150'] : $results[i]['120x90'];
                                    ww = 300;
                                    hh = 642;
                                    tipo = "portrate";
                                    ultimo = arr.length * nvueltas;
                                    break;
                                } else if (arr[j + bandera][0] == "tesugerimos") {
                                    $image = (typeof $results[i]['300x400'] !== 'undefined') ? $results[i]['300x400'] : (typeof $results[i]['300x150'] !== 'undefined') ? $results[i]['300x150'] : $results[i]['120x90'];
                                    ww = 300;
                                    hh = 600;
                                    tipo = "tesugerimos";
                                    ultimo = arr.length * nvueltas;
                                    break;
                                } else if (arr[j + bandera][0] == "negro") {
                                    $image = (typeof $results[i]['300x165'] !== 'undefined') ? $results[i]['300x165'] : (typeof $results[i]['300x150'] !== 'undefined') ? $results[i]['300x150'] : $results[i]['120x90'];
                                    ww = 300;
                                    hh = 169;
                                    tipo = "mm-negro";
                                    ultimo = arr.length * nvueltas;
                                    break;
                                }
                            }
                            
                            bandera++;
                            if (tipo == 'box_banner') {
                                elem = '<div class="item" data-options="[desktop=1, tablet=3, movil=2, desktop_visible=true, tablet_visible=false, movil_visible=false]">'
                                        + '<div class="box_banner_' + cubo + ' mm-container ' + tipo + ' grid-element" >'
                                            + '<div class="ads-300-x" style="opacity: 1;">'
                                                + '<div class="title-ads">publicidadida</div>'
                                                + '<div id="ban03_televisa">'
                                                    + '<div id="google_ads_iframe_/5644/es.televisa.lifestyle/entretenimiento/home_1__container__" style="border: 0pt none;">'
                                                        + '<iframe id="google_ads_iframe_/5644/es.televisa.lifestyle/entretenimiento/home_1" name="google_ads_iframe_/5644/es.televisa.lifestyle/entretenimiento/home_1" width="300%" height="225" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" src="javascript:&quot;<html><body style="background:transparent"></body></html>&quot;" style="border: 0px; vertical-align: bottom;"></iframe></div><iframe id="google_ads_iframe_/5644/es.televisa.lifestyle/entretenimiento/home_1__hidden__" name="google_ads_iframe_/5644/es.televisa.lifestyle/entretenimiento/home_1__hidden__" width="0" height="0" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" src="javascript:&quot;<html><body style="background:transparent"></body></html>&quot;" style="border: 0px; vertical-align: bottom; visibility: hidden; display: none;">'
                                                        + '</iframe>'
                                                    + '</div>'
                                            + '</div>'
                                        + '</div>'
                                    + '</div>';
                            } else if (tipo == 'superbanner') {
                                elem = '<div class="item" data-options="[desktop=1, tablet=3, movil=2, desktop_visible=true, tablet_visible=true, movil_visible=true]" style="width: '+ww+'px; ">'
                                            +'<div class="banners-desktop-1">'
                                            +'<span>Publicidad</span>'
                                            +'<div id="banner_'+banner+'">'
                                            +'</div>'
                                            +'</div>';
                                banner++;
                            } else if (tipo == 'portrate') {
                                elem = '<div class="item" data-options="[desktop=1, tablet=1, movil=1, desktop_visible=true, tablet_visible=false, movil_visible=false]" style="width: '+ww+'px; height: '+hh+'px;">' 
                                    + '<div class="ads-300-x portrait_banner_' + portrait + ' no-mobile no-tablet">'
                                            +'<div class="title-ads">publicidad</div>'
                                            +'<div id="ban03_televisa"></div>'
                                        +'</div>';
                            } else if (tipo == 'tesugerimos') {
                                elem = '<div class="item icarrusel" data-options="[desktop=1, tablet=1, movil=1, desktop_visible=true, tablet_visible=true, movil_visible=true]">'
                                       +'<article class="mv-container" id="masleido">'
                                           + '<div class="mv-rel-content-container">'
                                               + '<div class="mv-relacionado-container">'
                                               + '</div>'
                                               + '<div class="owl-nav">'
                                                   + '<div class="mv-button mv-prev iu-btn disabled">'
                                                       + '<i class="tvsagui-flechaizquierda"></i>'
                                                   + '</div>'
                                                   + '<div class="mv-button mv-next iu-btn">'
                                                       + '<i class="tvsagui-flechaderecha"></i>'
                                                   + '</div>'
                                               + '</div>'
                                           + '</div>'
                                         + '</article>'
                                        +'</div>';
                            } else {
                                /*elem = '<div class="item" data-options="[desktop=1, tablet=1, movil=1, desktop_visible=true, tablet_visible=true, movil_visible=true]">'
                                                + '<div class="mm-container '+tipo+' grid-element" style="opacity: 0;">'
                                                        + '<a href="'+categoryurl+'" class="mm-topic-a">'
                                                                + '<div class="mm-topic iu-btn">'
                                                                        + '<span>'+category+'</span>'
                                                                + '</div>'
                                                        + '</a>'
                                                        + '<a href="'+urls+'" class="mm-vinculo">'
                                                                + '<div class= "mm-img-container">'
                                                                        + '<img class="mm-img" href="#" src="'+$image+'" width="'+ww+'" height="'+hh+'" alt="'+title+'">'
                                                                        + icono_vf
                                                                + '</div>'
                                                                + '<h3 class="mm-content-title">'+title+'</h3>'
                                                                + '<div class="mm-text-container">'
                                                                        + '<p>'+ $results[i]['summary']+'</p>'
                                                                + '</div>'
                                                        + '</a>'
                                                        + '<div class="mm-social">'
                                                                + '<div href="#" class="mm-compartir"><span>Comparte la nota</span></div>'
                                                                + '<div href="#" class="mm-social-icons xxx" data-comm-share="true" data-comm-img="'+$image+'" data-comm-url="'+urls+'" data-comm-title="'+title+'">'
                                                                + '</div>'
                                                        + '</div>'
                                                + '</div>'
                                        + '</div>';*/
                                elem = '<div class="item" data-options="[desktop=1, tablet=1, movil=1, desktop_visible=true, tablet_visible=true, movil_visible=true]">'
                                            +'<div class="'+tipo+'">'
                                                +'<a href="'+urls+'">'
                                                    +'<div class="um-image">'
                                                        +'<img src="'+$image+'" width="'+ww+'" height="'+hh+'" alt="'+title+'">'
                                                        + icono_vf
                                                        +'<div class="um-border"></div>'
                                                    +'</div>'
                                                    +'<div class="um-title">'
                                                        +'<h3>'+title+'</h3>'
                                                    +'</div>'
                                                +'</a>'
                                            +'</div>'
                                        + '</div>';
                            }
                            if (urls !== null)
                                if ($("#iso [href='"+urls+"']").length === 0) {
                                    $container.isotope('insert', $(elem));
                                }
                            if (tipo == 'tesugerimos') {
                                try{
                                    mv.init();    
                                }catch(e){
                                    console.log(e);
                                }
                            }
                            $container.on('layoutComplete', function(){
                                if (w > mq_desktop) {
                                    func.media_desktop(func.mediaDesktop);
                                } else if (w > mq_tablet) {
                                    func.media_tablet(func.mediaTablet);
                                } else if (w > mq_movil) {
                                    func.media_smartphone(func.mediaSmartPhone);
                                }
                            });
                            inicio += 1;
                        }else {
                            finalizo++;
                        }
                    }
                    if (nvueltas >= pagination_vueltas || finalizo > 0) {
                        $('.btn_container').hide();
                        $('.infinite-scrolling').css("margin-bottom","24px");
                       
                    }else {
                        $('.btn_container').show();
                    }
                    var show = setTimeout(function() {
                        $(".item").children(0).css("opacity", 1);
                        $(".loader").css("display", "none");
                        $('.btn_container').css("padding-top", "0px");
                        gridFluido.grid();
                        //func.share_reload();
                        clearTimeout(show);
                        func.comunities_load();
                    }, 100);
                    var showb = setTimeout(function() {
                        clearTimeout(showb);
                        if ( banner > 0 ) {
                            if ($(window).width() > 947) {
                                bannerLoadDesktop();
                                console.log('bannerLoadDesktop');
                            }else if ($(window).width() > 648 && $(window).width() <= 947) {
                                bannerLoadTablet();
                                console.log('bannerLoadTablet');
                            }else if ($(window).width() > 320 && $(window).width() <= 647) {
                                bannerLoadMobile();
                                console.log('bannerLoadMobile');
                            }
                            
                        }
                    }, 1500);
                    $("#iso .item").resize(function() {
                        gridFluido.grid();
                    });
                }
            }else{
                $(".loader").css("display", "none");
            }
            if (w > mq_desktop) {
                func.mediaDesktopPortrait();
            }
        });
        return false;
    }
}

$(document).ready(function() {    
    $container = $('.layout'), 
    colWidth = gridFluido.colWidth(), 
    widthGutter = gridFluido.widthGutter(), 
    grid = gridFluido.grid(); 
    layuot_options = eval($container.data("options")),
    mq_desktop = parseInt(layuot_options[0]),
    mq_tablet = parseInt(layuot_options[1]),
    mq_movil = parseInt(layuot_options[2]),
    init = eval($container.data('autoinit')),
    w = $container.width();
    if (w >= mq_desktop) {
        pagination = eval($container.data('desktop'));
    } else if (w >= mq_tablet) {
        pagination = eval($container.data('tablet'));
    } else if (w >= mq_movil) {
        pagination = eval($container.data('smartphone'));
    }
    maxitems = parseInt(pagination[0]);
    if (parseInt(pagination[1]) > 0) $('.btn_container').show();
    $('.btn_container').on("click", gridFluido.loadData);
    if (init) $('.btn_container').trigger('click');
    $(window).on("resize", gridFluido.grid);
    $("#iso .item").resize(gridFluido.grid);
    gridFluido.grid();
    
});
window.onload = function() {
    gridFluido.grid();
}
