$( document ).ready(function() {

/*CreaciÃ³n de iframe videoplayer*/
    $(".ponte-de-pie .video-nt a.link").on("click", function(e) {
        e.preventDefault();
        /*Oculto cualquier player abierto*/
        if ($('.header-video').hasClass('fixed')) {
            //do fixed
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        } else {
            $('.ponte-de-pie .video-nt iframe').remove();
            $('.ponte-de-pie .video-nt a, .ponte-de-pie .video-nt img').css('display', 'block');
            /*Armo el iframe correspondiente*/
            var id_video = $(this).attr("href");
            $(this).siblings().css('display', 'none');
            $(this).css('display', 'none');
            if ($(window).width() > 948) {
                $(this).parent().append('<iframe src="http://amp.televisa.com/embed/embed.php?id=' + id_video + '&amp;w=624&amp;h=351&amp;autoplay=true&amp;canal=es.esmas.video|reality-shows|ponte-de-pie|home&subcanal=1011" frameborder="0" scrolling="0" class="iframe-videoplayer"></iframe>');
            } else {
                v_ancho = $(this).width();
                v_alto = parseInt((v_ancho * 9) / 16);
                $(this).parent().append('<iframe style="width:' + v_ancho + 'px;height:' + v_alto + 'px;" src="http://amp.televisa.com/embed/embed.php?id=' + id_video + '&amp;w=' + v_ancho + '&amp;h=' + v_alto + '&amp;autoplay=false&amp;canal=es.esmas.video|reality-shows|ponte-de-pie|home&subcanal=1011" frameborder="0" scrolling="0" class="iframe-videoplayer"></iframe>');
            }
        }
    });
    $(".algo").on("click", function(e) {
        if ($('.header-video').hasClass('fixed')) {
            //do fixed
            $("html, body").animate({
                scrollTop: 0
            }, 500);
            return false;
        }
    });
/* ./CreaciÃ³n de iframe videoplayer*/

/*CreaciÃ³n de iframe fotoplayer*/
$('.ponte-de-pie .photo-nt a.link').on("click", function(e) {
        e.preventDefault();
        var url_fp = $(this).attr("href").replace("http://", "http://m.");;
        var photo_iframe = url_fp;
        if ($(window).width() < 648) {
            var ancho = $('.iu-cuerpo-nt .photo-nt').width();
            var alto = $('.iu-cuerpo-nt .photo-nt').height();
            $(this).parent().html('<iframe style="width:' + ancho + 'px;height:' + alto + 'px;background:black;" src="' + photo_iframe + '" frameborder="0" scrolling="0" class="iframe-photoplayer-article"></iframe>');
        } else {
            $(this).parent().html('<iframe style="width:624px;height:351px;background:black;" src="' + photo_iframe + '" frameborder="0" scrolling="0" class="iframe-photoplayer-article"></iframe>');
        }
    });
$('.ponte-de-pie .photo-nt a.link').trigger('click');
/* ./CreaciÃ³n de iframe fotoplayer*/

/* Shares redes sociales*/
 if ($("html.no-desktop").length) {
    $('.ponte-de-pie .shares a.red-social').addClass("tap");
}
$(document).on('click', '.ponte-de-pie .shares a.red-social', function(e) {
    e.preventDefault();
    if($(this).hasClass('tap')){
        if (!$(this).siblings().is(":visible")) {
            $('.ponte-de-pie .shares .div_facebook').hide();
            $('.ponte-de-pie .shares .div_twitter').hide();      
            $(this).siblings().show('slow');
        }else{            
            $(this).siblings().hide('slow');
        }
    }
});
/* ./Shares redes sociales*/

/*Countdown*/
$(function () {
    var austDay = new Date();
    var anio = $('.ponte-de-pie #countdown').data('anio');
    var mes = $('.ponte-de-pie #countdown').data('mes');
    var dia = $('.ponte-de-pie #countdown').data('dia');
    austDay = new Date( anio, mes - 1, dia);
    shortly = new Date(); 
    shortly.setSeconds(shortly.getSeconds() + 5.5); 
    $('.ponte-de-pie #countdown').countdown({until: austDay, compact: true, layout: '<p class="altas">Pr&oacute;ximamente en&nbsp;</p><p> {dn}{sep}{hnn}{sep}{mnn}{sep}{snn}</p> {desc}', 
    description: '<p>hrs</p>', expiryText: '<div class="aviso"><i class="logo-tvsa no-desktop"><a href="http://www.televisa.com/canal-de-las-estrellas/" class="link-logo" alt="Canal de las Estrellas">Canal de las Estrellas</a></i><p class="lanzamiento no-margin">Domingo <i class="logo-tvsa desktop"><a href="http://www.televisa.com/canal-de-las-estrellas/" class="link-logo" alt="Canal de las Estrellas">Canal de las Estrellas</a></i></p><p class="lanzamiento"> 8:00 de la noche</p></div>'});
});
/* ./Countdown*/


$(this).footerTelevisa({
    //Activa o desactiva elementos del footer
    'HomePage': false,
    'SocialMobileNav': false,
    'CategoriesNav': false,
    'CategoriesMobileNav': false,
    'BtnReturnMobile': false,
    'SectionsCorpNav': true,
    'LegalNav': true,
    //Carga LazyLoad, se carga el footer hasta que el usuario scrollea hasta el header
    'LazyLoad': false,
    'Force':true
});


if ( $(".header-video").length ) {
    var elem = $(".header-video"), maxTop = elem.parent().offset().top+elem.parent().outerHeight(true), maxH = elem.outerHeight(true);
    var video_flotante = function (e) {
        if ( elem.length ) {
            if ( $("html").hasClass('no-mobile') ) {
                
                if ( $(window).scrollTop() > maxTop && $(window).width() > 947 ) {
                    elem.addClass('fixed');
                    elem.parent().css('padding-top', maxH+40);
                    var v_ancho = elem.find(".video-nt").width();
                    var v_alto = parseInt((v_ancho * 9) / 16);
                    elem.find('.iframe-videoplayer').css({ 'width': v_ancho+"px", 'height': v_alto+"px" });
                }else{
                    elem.removeClass('fixed');
                    elem.parent().css('padding-top', '');
                    elem.find('.iframe-videoplayer').css({ 'width': '', 'height': '' });
                }
            }
        }
    }
    $(window).scroll(video_flotante);
    video_flotante();
    $(window).resize(function(){
        maxTop = elem.parent().offset().top+elem.parent().outerHeight(true);
        if ( $("html").hasClass('no-mobile') && $(window).width() > 947 ) {
            if ( $(window).scrollTop() > maxTop ) {
                elem.addClass('fixed');
                elem.parent().css('padding-top', maxH+40);
                var v_ancho = elem.find(".video-nt").width();
                var v_alto = parseInt((v_ancho * 9) / 16);
                elem.find('.iframe-videoplayer').css({ 'width': v_ancho+"px", 'height': v_alto+"px" });
            }else{
                elem.removeClass('fixed');
                elem.parent().css('padding-top', '');
            } 
        }else{
            elem.removeClass('fixed');
            elem.css('display', 'block');
            elem.parent().css('padding-top', '');
        }
    });
}

});