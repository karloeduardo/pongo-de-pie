$('document').ready(function() {
    $.ajaxSetup({
      cache: true
      });

    var obten_url= $(location).attr('href').toLowerCase();
    if($("#participantes").length){
      var tmp_capitanes ="";
      $.getScript("http://especiales.televisa.com/me-pongo-de-pie/of_equipos.html").done(function(script, textStatus) {
          for(var i = 0; i<coaches.length; i++)
          {
            if (obten_url!=coaches[i]["link"]){
              tmp_capitanes+='<section class="participantes-container">';
                  tmp_capitanes+='<div class="contenedor-doble">';
                      tmp_capitanes+='<a href="'+coaches[i]["link"]+'">';
                          tmp_capitanes+='<div class="foto-principal">';
                              tmp_capitanes+='<img alt="'+coaches[i]["nombre"]+'" src="'+coaches[i]["img_150x304"]+'" class="tvsaimg-loaded">';
                          tmp_capitanes+='</div>';
                          tmp_capitanes+='<span class="texto-principal">'+coaches[i]["nombre"]+'</span>';
                      tmp_capitanes+='</a>';
                  tmp_capitanes+='</div>';
                  tmp_capitanes+='<div class="contenedor-doble">';
                  console.log("players: "+players.length);
                  console.log("player[i]: "+players[i].length);
                  for(var j=0; j<players[i].length;j++){
                    if(players[i][j]){
                      tmp_capitanes+='<div class="part-unidad-minima">';
                          tmp_capitanes+='<a href="'+players[i][j]["link"]+'">';
                              if (players[i][j]["status"]){
                              tmp_capitanes+='<span class="etiqueta">'+players[i][j]["status"]+'</span>';
                              }
                              tmp_capitanes+='<img alt="'+players[i][j]["nombre"]+'" src="'+players[i][j]["img_150x150"]+'" data-src="'+players[i][j]["img_150x150"]+'" class="tvsaimg-loaded">';
                              tmp_capitanes+='<div class="part-pleca">';
                                  tmp_capitanes+='<span>'+players[i][j]["nombre"]+'</span>';
                              tmp_capitanes+='</div>';
                          tmp_capitanes+='</a>';
                      tmp_capitanes+='</div>';
                    }
                  }
                  tmp_capitanes+='</div>';
              tmp_capitanes+='</section>';
            }
          }
          $(".grid-participantes").prepend(tmp_capitanes);
        }).fail(function(jqxhr, settings, exception) {
        console.log(uiName.name + "ERROR! No se puede cargar of_equipos.html");
      });
    }

    if($("#jueces").length){
      var tmp_jueces ="";
      $.getScript("http://especiales.televisa.com/me-pongo-de-pie/of_equipos.html").done(function(script, textStatus) {
            if(juez_titulo){
              tmp_jueces+='<h3 class="title-mas-vistos">'+juez_titulo+'</h3>';
            }else{tmp_jueces+='<h3 class="title-mas-vistos">JUECES</h3>';}
            tmp_jueces+='<ul class="container-mas-vistos">';
            for(var i = 0; i<jueces.length; i++)
            {
                tmp_jueces+='<li>';
                    tmp_jueces+='<a href="'+jueces[i]["link"]+'">';
                        tmp_jueces+='<img alt="'+jueces[i]["nombre"]+'" src="'+jueces[i]["img_300x112"]+'" data-src="'+jueces[i]["img_300x112"]+'" class="tvsaimg-loaded">';
                        tmp_jueces+='<div class="hoverdecolor"></div>';
                        tmp_jueces+='<div class="centrador">';
                            tmp_jueces+='<h4>'+jueces[i]["nombre"]+'</h4>';
                            tmp_jueces+='<span class="conductor">';
                        tmp_jueces+='</span></div>';
                    tmp_jueces+='</a>';
                tmp_jueces+='</li>';
              }
            tmp_jueces+='<div class="clear-tablet"></div>';
            tmp_jueces+='</ul>';

            $("#jueces").prepend(tmp_jueces);
        }).fail(function(jqxhr, settings, exception) {
        console.log(uiName.name + "ERROR! No se puede cargar of_equipos.html");
      });
    }

    if($("#conductores").length){
      var tmp_cond ="";
      $.getScript("http://especiales.televisa.com/me-pongo-de-pie/of_equipos.html").done(function(script, textStatus) {
            if(cond_titulo){
              tmp_cond+='<h3 class="title-mas-vistos">'+cond_titulo+'</h3>';
            }else{tmp_cond+='<h3 class="title-mas-vistos">JUECES</h3>';}
            tmp_cond+='<ul class="container-mas-vistos">';
            for(var i = 0; i<conductores.length; i++)
            {
                tmp_cond+='<li>';
                    tmp_cond+='<a href="'+conductores[i]["link"]+'">';
                        tmp_cond+='<img alt="'+conductores[i]["nombre"]+'" src="'+conductores[i]["img_300x112"]+'" data-src="'+conductores[i]["img_300x112"]+'" class="tvsaimg-loaded">';
                        tmp_cond+='<div class="hoverdecolor"></div>';
                        tmp_cond+='<div class="centrador">';
                            tmp_cond+='<h4>'+conductores[i]["nombre"]+'</h4>';
                            tmp_cond+='<span class="conductor">';
                        tmp_cond+='</span></div>';
                    tmp_cond+='</a>';
                tmp_cond+='</li>';
              }
            tmp_cond+='<div class="clear-tablet"></div>';
            tmp_cond+='</ul>';

            $("#jueces").prepend(tmp_cond);
        }).fail(function(jqxhr, settings, exception) {
        console.log(uiName.name + "ERROR! No se puede cargar of_equipos.html");
      });
    }

    if($("#participantes2").length){
      var tmp_part2 ="";
      $.getScript("http://especiales.televisa.com/me-pongo-de-pie/of_equipos.html").done(function(script, textStatus) {
            for(var i = 0; i<coaches.length; i++){
                for(var j=0; j<players[i].length;j++){
                    tmp_part2+='<div class="part-unidad-minima iu-participantes2">';
                        tmp_part2+='<a href="'+players[i][j]["link"]+'">';
                            if (players[i][j]["status"]){
                              tmp_part2+='<span class="etiqueta">'+players[i][j]["status"]+'</span>';
                            }
                            tmp_part2+='<img alt="'+players[i][j]["nombre"]+'" src="'+players[i][j]["img_150x150"]+'" data-src="'+players[i][j]["img_150x150"]+'" class="tvsaimg-loaded">';
                            tmp_part2+='<div class="part-pleca">';
                                tmp_part2+='<span>'+players[i][j]["nombre"]+'</span>';
                            tmp_part2+='</div>';
                        tmp_part2+='</a>';
                    tmp_part2+='</div>';
                }
            }
            $("#participantes2").prepend(tmp_part2);
        }).fail(function(jqxhr, settings, exception) {
        console.log(uiName.name + "ERROR! No se puede cargar of_equipos.html");
      });
    }

});